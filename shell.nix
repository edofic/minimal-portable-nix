let

  default = import ./.;

in

  default.env // {
    withProfiling = default.withProfiling.env;
  }
