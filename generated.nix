{ mkDerivation, base, stdenv }:
mkDerivation {
  pname = "minimal-portable-nix";
  version = "0.1.0.0";
  src = ./src;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base ];
  license = stdenv.lib.licenses.bsd3;
}
