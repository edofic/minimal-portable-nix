# minimal portable nix environment for a haskell project

What I have so far (not working yet)

```bash
INTERACTIVE=$(nix-shell -vv --run true 2>&1| grep -Eo "/nix.+interactive[^’]+")
RESULT=$(nix-build --no-out-link)
EXTRAS=$(nix-build --no-out-link -A extras .)
REFS=$(nix-store --realise $(nix-store --query --references $INTERACTIVE))
REQUISITES=$(nix-store --query --requisites $REFS $EXTRAS $RESULT)
nix-store --export $EXTRAS $REFS $REQUISITES $RESULT > shell.closure
```

## to reproduce

```bash
docker run --rm -it -v $(pwd):/src  nixos/nix bash
cd /src
nix-store --import < shell.closure
nix-build  # should not download anything
nix-shell  # should not download anything
```

