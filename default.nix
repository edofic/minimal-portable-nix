let

  nixpkgsDrv = import ./fetch-nixpkgs.nix;
  pkgs = import nixpkgsDrv {};

  package = import ./generated.nix;

  asterix_data_repo = pkgs.fetchgit {
    url = "https://github.com/zoranbosnjak/asterix-data.git";
    rev = "04e0b5bae0fc2060d27d4467c2a25a4bf09b23fe";
    sha256 = "0g8i98a7m1gaag00jrispkvxdwyrliwp6f1py1wr4jch9cka0yg2";
  };

  asterix_data = import asterix_data_repo { inherit pkgs; };

  asterix_data_xmls = "${asterix_data}/asterix-data/xml";

  mkDependencies = self: {
    # private packages here
  };

  haskellPackages = pkgs.haskellPackages.override {
    overrides = self: super: mkDependencies self;
  };

  profilingPackages = pkgs.haskellPackages.override {
    overrides = self: super: mkDependencies self // {
      mkDerivation = args: super.mkDerivation (args // {
        enableLibraryProfiling = true;
      });
    };
  };

  mkEnv = haskellPackages:
    let derivation = haskellPackages.callPackage package {};
    in  derivation.overrideDerivation (oldAttrs: {
      preBuild = ''
        export ASTERIX_DATA_PATH=${asterix_data_xmls}
      '';
    }) // {
      env = derivation.env.overrideAttrs (oldAttrs: {
        STATIC_DIR = "monitor";
        ASTERIX_DATA_PATH = asterix_data_xmls;
        buildInputs = [ pkgs.cabal-install ];
      });
    };

   extras = pkgs.stdenv.mkDerivation {
     name = "extras";
     buildCommand = ''
       mkdir $out
       cd $out
       ln -s ${nixpkgsDrv}
       ln -s ${asterix_data_repo}
     '';
   };

in

  mkEnv haskellPackages // {
    inherit extras;
    withProfiling = mkEnv profilingPackages;
  }
